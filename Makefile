
SOURCE = main

SRC = $(SOURCE).c 

OBJ = $(SOURCE).o

EXE = $(SOURCE).exe

CC = gcc

all: $(OBJ) $(EXE)

$(OBJ): $(SRC)
	$(CC) -c $(SRC) $(OPT)

$(EXE): $(OBJ)
	$(CC) -o $(EXE) $(OBJ)

clean:
	rm *.o
