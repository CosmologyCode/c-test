#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void) 
{

  int i;

  double *p;
  double x;

  p = (double *) malloc(10*sizeof(double)) ;

  for(i=0;i<10;i++)
    {
      x = (double) i+1;
      p[i]=x*x+x;

      printf(" i:= %g; y:= %g\n",x,p[i]);
    }
  
  free(p);
  
  p = (double *) malloc(5*sizeof(double)) ;

  for(i=0;i<5;i++)
    {
      x = (double) i+1;
      p[i]= pow(x,2);

      printf(" i:= %g; y:= %g\n",x,p[i]);
    }
  
  free(p);


  return 0;

}


    
